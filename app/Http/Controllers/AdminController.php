<?php

namespace App\Http\Controllers;
use App\Models\Tag;
use App\Models\Category;
use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    public function index(Request $request){

        if(!Auth::check() && $request->path() != 'login'){
            // ddd($request->path());
            return redirect('/login');
        }
        if(!Auth::check() && $request->path() == 'login'){
            return view('welcome');
        }


        $user = Auth::user();
        if($user->userType == 'User'){
            redirect('/login');
        }
        if($user->userType == 'Admin'){
            redirect('/');
        }
       return $this->chackPermission($user,$request);
    //     return view('f_404');
    //    return view('/welcome');
    }
    public function chackPermission($user,$request){
        
        $pertr = false;
         $pat = json_decode($user->role);
         $pat = json_decode($pat->permission);
         if(!isset($pat)) return view('/welcome');
         foreach($pat as $pa){
            // dd($pa,$request->path());
             if($pa->name == $request->path()){

                if($pa->read){
                     $pertr = true;
                     break;
                 }
             }
         }
         if($pertr){
             return view('/welcome');
         }
         return view('f_404');     
    }
    public function logouts(){
        if(Auth::check()){
            Auth::logout();
        }
        // ddd('sdvsvvsd');
        return redirect('/login');
    }

    public function addTag(Request $request){
        $request->validate([
            'tagName'=>"required"
        ]);
        return Tag::create([
            'tagName'=>$request->tagName
        ]);   
    }

    public function editTag(Request $request){
        $request->validate([
            'tagName'=>"required"
        ]);
        Tag::where('id',$request->id)->update([
            'tagName'=>request('tagName')
        ]);
    }

    public function getTag(){
        return Tag::orderBy('id','desc')->get();
    }

    public function deleteTag(Request $request){
        $request->validate([
            'id'=>"required"
        ]);
        return Tag::where('id',$request->id)->delete();
    }

    public function deleteCategory(Request $request){
        //dd($request->id);
       if(isset($request->id)){
        return Category::where('id',$request->id)->delete(); 
       }else return 'Error';
    }

    public function upload(Request $request){
        $this->validate($request,[
            'file'=>'mimes:jpeg,png,jpg'
        ]);
        $nickName = time().".".$request->file->extension();
        $request->file->move(public_path('upload'),$nickName);
        return $nickName;
    }
    public function deleteCimage(Request $request){
       $name = request('imgName');
       $path = public_path().'/upload/'.$name; 
       if(file_exists($path)){
           @unlink($path);
       }
        return 'done';
 
    }
    public function deleteFileFromServer(){

    }

    public function addCtegory(Request $request){
        $request->validate([
            'categoryName'=>['required'],
            'iconImage'   =>['required']
        ]);
         return Category::create([
              'categoryName'=>request('categoryName'),
              'iconImage' =>request('iconImage')
          ]);
    }
    public function getCategory(){
        return Category::orderBy('id','desc')->get();
    }

    public function editCategory(Request $request){
        $request->validate([
            'categoryName'=>"required",
            'iconImage'=>"required"
        ]);
       return Category::where('id',$request->id)->update([
            'categoryName'=>request('categoryName'),
            'iconImage' => request('iconImage')
        ]);
        
    }
    public function getAdminUsers(){
        return User::where('userType','!=','User')->get();
    }

    public function creatUser(Request $request){
        $park = $request->validate([
            'name'=>'required',
            'email'=>'bail|required|email|unique:users',
            'password'=>'required|min:6',
            'role_id'=>'required'
        ]);
        $password=bcrypt($request->password);
         return  User::create($park);
    }

    public function EditUser(Request $request){
        
        $valid = $request->validate([
            'id'=>'required',
            'name'=>'required',
            'email'=>"bail|required|email|unique:users,email,$request->id",
            // 'password'=>'required|min:6',
            'userType'=>'required'
        ]);
        if(trim($request->password)!=''){
            $password=bcrypt($request->password);
            $valid['password']=$password;
        }
        return User::where('id',$request->id)->update($valid);
    }
    public function loginUser(Request $request){ 
        
        $request->validate([
            'email'=>'required|email',
            'password'=>'required|min:6'
        ]);
        
        // dd(Auth::attempt(['email'=>$request->email,'password'=>$request->password]));
        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password])){
           $user = Auth::user();
      
            // \Log::info($user);
            // \Log::info("salom");
            // $user->role->isAdmin == 0 
           if(0){
               Auth::logout();
               return response()->json([
                'msg'=>'Email or Password is incorrect!'
            ],401); 
           }
            return response()->json([
                'msg'=>"You are logged in"
            ],200);
        }else{
            return response()->json([
                'msg'=>'Email or Password is incorrect!'
            ],401);
        }
    }
  // Roles Function //////

  public function getRoles(){
       return Role::orderBy('id','desc')->get();
  }
  public function addRoles(Request $request){
     $data = $request->validate([
          'roleName'=>'required'
      ]);
     return Role::create($data);
  }
  public function editRole(Request $request){
    $data = $request->validate([
        'roleName'=>'required'
    ]);
    return Role::where('id',$request->id)->update($data);
  }
  public function assignRole(Request $request){
      $request->validate([
          'permission'=>'required',
          'role_id'=>'required'
      ]);
      return Role::where('id',$request->role_id)->update(['permission'=>$request->permission]);
  }
}
