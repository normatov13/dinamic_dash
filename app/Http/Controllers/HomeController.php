<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use App\Mail\Contactme;
use App\Notifications\PaymentNotification;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function send(){
        request()->validate([
          'email'=>['required','email']
        ]);

        Mail::to(request('email'))
        ->send(new Contactme());

    //  Mail::raw('it Works !', function($message){
    //   $message->to(request('email'))
    //           ->Subject('Hello There');
    //     });
       
        return redirect('/contact')->with('message','Email send Success');
    }
    public function notification(){
        Notification::send(request()->user(),new PaymentNotification());
    }

 public function test(){
     return response()->json([
         'salom'=>"Hello WOrld" 
     ]);
 }

 public function testvue(){
     return response()->json([
        "msg"=>"It is testvue Functions !!!"
     ],422);
 }



}
