<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class checkAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        \Log::info("it is CheckAdmin Middleware ! ==". $request->path());
        //   return {$request->path()}; 
        if($request->path() == "app/login_user"){
            return $next($request);  
        }
        if(!Auth::check()){
            return response()->json([
                'msg'=>'You have not access this route...'
            ],402);
        }
        $user=Auth::user();

        if($user->role->isAdmin == 0){
            return response()->json([
                'msg'=>'You have not access this route...'
            ],402); 
        }
        return $next($request);
    }
}
