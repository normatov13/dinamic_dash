import Vuex, { mapGetters } from 'vuex'
export default{
    data(){
        return {

        }
    },

    methods:{
       async callApi(method,url,dataObj){
try {
   return await axios({
    method: method,
    url: url,
    data: dataObj
         })
    } catch (e) {
        console.log(e);
        return e.response;
        }
            } ,
            
            i (desc,title="Hey") {
                this.$Notice.info({
                    title: title,
                    desc: desc
                });
            },
            s (desc,title="Great !") {
                this.$Notice.success({
                    title: title,
                    desc: desc
               });
            },
            w (desc,title="Oops !") {
                this.$Notice.warning({
                    title: title,
                    desc: desc
              });
            },
            e (desc,title="Oops !") {
                this.$Notice.error({
                    title: title,
                    desc: desc
                });
            },
            swr (desc= "Something went wring ! Please try again.",title="Oops !") {
                this.$Notice.error({
                    title: title,
                    desc: desc
                });
            },
            checkPermission(key){
                if(!this.userPremission)return true;
               let ispermited = false
                for(let i of this.userPremission){
                  if(this.$route.name == i.name){
                      if(i[key]){
                          ispermited=true
                          break
                        }else break;
                  }
                }
                return ispermited
            }
                },

      computed:{
      ...mapGetters({
          'userPremission':'getUserPermission'
      }),
      isReadPermitted(){
        return this.checkPermission('read');
      },
      isWritePermitted(){
        return this.checkPermission('write');
      },
      isUpdatePermitted(){
        return this.checkPermission('edit');
      },
      isDeletePermitted(){
        return this.checkPermission('delete');
      }
    }

                    }

