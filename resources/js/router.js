import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

import firstpage from './components/pages/MyFirstVuePage'
import hooks from './components/pages/basic/hooks'
import main from './components/mainapp'
import method from './components/pages/basic/method'
import newpage from './components/pages/Newpage'
import vuexAll from './vuex2/vuexAll'
// Projects
import home from './components/pages/home'
import tags from './admin/pages/tags'
import category from './admin/pages/category'
import allcom from './vuex/usecom'
import adminusers from './admin/pages/adminusers'
import login from './admin/pages/login'
import role from './admin/pages/role'
import assignRole from './admin/pages/assignRole'
import createBlog from './admin/pages/createBlog'

const routes = [
    // Projects
   
     {
         path:'/', 
         component : home,
         name:'home'
     },
     {
        path:'/adminusers', 
        component : adminusers,
        name:'adminusers'
     },
     {
         path:'/vuex2',
         component:vuexAll,
         name:'vuexAll'
     },
      {
         path:'/createBlog',
         component:createBlog,
         name:'createBlog'
     },
     {
         path:"/tags",
         component : tags,
         name:'tags'
     },
     {
         path:'/category',
         component:category,
         name:'category'
     },
     {
         path:'/allcom',
         component :allcom,
         name:'allcom'
     },
     {
        path:'/login',
        component:login,
        name:'login'
     },
     {
      path:'/role',
      component:role,
      name:'role'
     },
     {
        path:'/assignRole',
        component:assignRole,
        name:'assignRole'
       },
// practise
    {
        path :'/Vue-router-new',
        component : firstpage
    },
    {
        path :'/new-route',
        component : newpage
    },
    // vue hooks
    {
        path :'/hooks',
        component : hooks
    },
    // some methods
    {
        path :'/methods',
        component : method
    },
    {
        path :'/admin',
        component : main
    }

]

export default new Router({
    mode : 'history',
    routes
})