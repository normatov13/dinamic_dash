import Vue from 'vue'
import Vuex, { Store } from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
    state :{
       com:100,
       user:false,
       DeleteModalObj:{
        showDeleteModel:false,
        data:null,
        is:-1,
        deleteUrl:'',
        isDelete:false
       },
       userPermission:null
    },

    getters :{
        getCounter(st){
            return st.com;
        },
        getDeleteModal(st){
            return st.DeleteModalObj;
        },
        getUserPermission(st){
            return st.userPermission;
        }
    },

    mutations:{
        userPermission(st,data){
          st.userPermission = data ;
        },
        changeCounter(st,amount){
      st.com += amount;
        },
        deleteModalCategory(st,data){
            st.DeleteModalObj=data;
        //  st.DeleteModalObj.isDelete=true 
        },
        setDeleteModal(st,data){
            const ter={
                showDeleteModel:false,
                data:null,
                is:st.DeleteModalObj.is,
                deleteUrl:'',
                isDelete:data   
            }
          st.DeleteModalObj = ter
        //   st.DeleteModalObj.isDelete=true
        },
        updateUsetr(st,data){
            st.user = data;
        }
    },

    actions:{
        incriseConter({commit},data){
              commit('changeCounter',data)
        }
    }
})