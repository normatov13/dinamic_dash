import Vue from 'vue'
import Vuex, { Store } from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
    state :{
        counter:1000,
        deleteModalObj:{
            showDeleteModel:false,
            deleteUrl:'',
            data : null,
            index: -1
        }
    },
// Gettersni vazifasi state dagi biror objectni componentalarga uzatish
    getters:{
       getCounter(state){
           return state.counter
       },
       getDeleteModalObj(state){
         return state.deleteModalObj
       }
    },
// commit qilingnanda keladigan function  --  bu mutations  !!!!!
    mutations:{
        changeTheCounter(state,data){
           this.state.counter += data ;
        }
    },
// Dispatch qilingnanda keladigan function  --  bu actions  !!!!!
    actions:{
         changeStoreAction({commit},data){
         commit('changeTheCounter',data)
        }
    }

})