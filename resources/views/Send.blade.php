@extends('layouts.app')

@section('content')

<form action="/send" method="POST" >
@csrf
<div class="container">
<label for="em">Send Email Me</label>
<input type="text" required name="email"  id="em" class="@error('email') is-invalid @enderror form-control">
@error('email')
<div class="text-danger is-invalid">{{$message}}</div>
@enderror

<input type="submit" value="Submit" class="mt-4 btn  btn-primary" >
@if(session('message'))
    <div class="text-success">{{session('message')}}</div>
@endif
</div>
</form>

@endsection