<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Full Stack Vue Devolopment</title>
        <link href="{{ asset('/css/all.css') }}" rel="stylesheet"> 

     <script>
          (function(){
            window.Laravel = {
              csrfToken : '{{ csrf_token() }}'
            }
          })();
     </script>

    </head>
    <body >

  <div id="app">
          @if(Auth::check())
        <mainapp :user="{{Auth::user()}}" :permission="{{Auth::user()->role->permission}}"></mainapp>
        @else
        <mainapp :user="false"></mainapp>
        @endif
        <router-view></router-view>
     </div>
       <script src="{{mix('/js/app.js')}}"></script>
    </body>
</html>
