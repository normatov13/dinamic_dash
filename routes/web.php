<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Redis;
use App\Http\Middleware\checkAdmin;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/logout',function(){
//     auth()->user()->logout();
//      return "<p> You are Now logout !!! </p>";
// // });
// Route::get('/',
// function () {

//     // App\Jobs\sendMessage::dispatch("TEST MESSAGE");
//     // $salom = new \App\Container;
    
//     // $salom->bind('example',function(){
//     //     return new App\Example;
//     // });
//     // ddd($salom);
//      return view('welcome');
// });

Route::get('/testonly',function(){
    return "csd";
});
//////////////// Start Vue Full Stack Devolopment   /////////////////////////
Route::prefix('app')->middleware([checkAdmin::class])->group(function(){
Route::post('/created_tag',[AdminController::class,'addTag']);
Route::get('/get_Tag',[AdminController::class,'getTag']);
Route::post('/edit_tag',[AdminController::class,'editTag']);
Route::post('/delete_Tag',[AdminController::class,'deleteTag']);
Route::post('/upload',[AdminController::class,'upload']);
Route::post('/delete_img',[AdminController::class,'deleteCimage']);
Route::post('/created_category',[AdminController::class,'addCtegory']);
Route::get('/get_Category',[AdminController::class,'getCategory']);
Route::post('/edit_category',[AdminController::class,'editCategory']);
Route::post('/delete_category',[AdminController::class,'deleteCategory']);
Route::post('/created_adminUser',[AdminController::class,'creatUser']);
Route::get('/get_UserAdmin',[AdminController::class,'getAdminUsers']);
Route::post('/edit_userAdmin',[AdminController::class,'EditUser']);
Route::post('/login_user',[AdminController::class,'loginUser']);

//roles eouter
Route::get('/get_Roles',[AdminController::class,'getRoles']);
Route::post('/created_Role',[AdminController::class,'addRoles']);
Route::post('/edit_Role',[AdminController::class,'editRole']);
Route::post('/assign_Role',[AdminController::class,'assignRole']);

});

Route::get('/logouts',[AdminController::class,'logouts']);

Route::get('/',[AdminController::class,'index']);
Route::get('{any}',[AdminController::class,'index']);


Route::get('/vue', [HomeController::class,'test']);
// Route::get('{slug}',function(){
//     return view('welcome');
// });









//////////////// And Vue Full Stack Devolopment   /////////////////////////





Route::get('/dart/show',
function () {
    return view('Notification');
});

Route::post('/send',[HomeController::class,'send']);
Route::post('/Notif',[HomeController::class,'notification']);


Route::get('/contact',function(){
 return view('Send');
});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])
->name('home')
->middleware('auth');

Route::get('/logout',function(){
   return auth()->logout();
});